import datetime
import json
import os
from urllib.request import urlopen

class Result(object):
    def __init__(self, short_text, full_text, icon, urgent):
        self.short_text = short_text
        self.full_text = full_text
        self.icon = icon
        self.urgent = urgent
        self.results = {
          'short_text' : self.short_text + " " + self.icon,
          'full_text'  : self.full_text + " " + self.icon,
          'urgent'     : self.urgent
        }
    
    @property
    def short_text(self):
        return self._short_text
    
    @short_text.setter
    def short_text(self, short_text):
        self._short_text = short_text
    
    @property
    def full_text(self):
        return self._full_text
    
    @full_text.setter
    def full_text(self, full_text):
        self._full_text = full_text
    
    @property
    def icon(self):
        return self._icon
    
    @icon.setter
    def icon(self, icon):
        if len(icon) != 1:
            raise ValueError('Icon value must be a single character')
        self._icon = icon
    
    @property
    def urgent(self):
        return self._urgent
    
    @urgent.setter
    def urgent(self, urgent):
        self._urgent = bool(urgent)
    
    def get_dict(self):
        return(self.results)


class Wgt_Clock(object):
    def __init__(self, params):
        self.date_fmt = params["date_format"]
    
    def create_result(self):
        timestring = self.get_timestring()
        result = Result(timestring, timestring, "\uF017", False)
        return(result)
    
    def get_timestring(self):
        now_dt = datetime.datetime.now()
        now_fmt = now_dt.strftime(self.date_fmt)
        return(now_fmt)
    
    @property
    def result(self):
        return self._result
    
    @result.setter
    def result(self, result):
        self._result = result


class Wgt_Battery(object):
    def __init__(self, params):
        self.bat_id = params["bat_id"]
    
    def create_result(self):
        batstring = self.get_batstring()
        baticon = self.get_baticon()
        result = Result(batstring, batstring, baticon, False)
        return(result)
    
    def get_batstring(self):
        bat_capacity = "/sys/class/power_supply/BAT" + self.bat_id + "/capacity"
        cap_file = open(bat_capacity, 'r')
        cap = str(cap_file.readline().rstrip("\n"))
        cap_file.close()
        cap_txt = cap + "%"
        return(cap_txt)
    
    def get_baticon(self):
        icons = {
          'Charging'    : '\uF0D8',
          'Discharging' : '\uF0D7',
          'Unknown'     : '\uF1E6'
        }
        bat_status = "/sys/class/power_supply/BAT" + self.bat_id + "/status"
        stat_file = open(bat_status, 'r')
        stat = stat_file.readline().rstrip("\n")
        stat_file.close()
        return(icons[stat])
    
    @property
    def result(self):
        return self._result
    
    @result.setter
    def result(self, result):
        self._result = result


class Wgt_Weather(object):
    def __init__(self, params):
        self.api_key = params['api_key']
        self.city_id = params['city_id']
        self.api_url = "http://api.openweathermap.org/data/2.5/weather"
    
    def create_result(self):
        weather_data = self.call_api(self.api_url, self.api_key, self.city_id)
        weatherstring = self.get_weatherstring(weather_data)
        weathericon = self.get_weathericon(weather_data)
        result = Result(weatherstring, weatherstring, weathericon, False)
        return(result)
    
    def get_weatherstring(self, weather_data):
        temperature_kelvin = float(weather_data['main']['temp'])
        temperature_celsius = int(temperature_kelvin - 273.15)
        suffix = "°C"
        weatherstring = str(temperature_celsius) + suffix
        return(weatherstring)
    
    def get_weathericon(self, weather_data):
        icons = {
          "bolt"          : "\uF0E7",
          "showers"       : "\uF740",
          "showers_day"   : "\uF743",
          "showers_night" : "\uF73C",
          "rain"          : "\uF73D",
          "snow"          : "\uF2DC",
          "fog"           : "\uF75F",
          "wind"          : "\uF72E",
          "clear_day"     : "\uF185",
          "clear_night"   : "\uF186",
          "cloudy_day"    : "\uF6C4",
          "cloudy_night"  : "\uF6C3",
          "cloudy"        : "\uF0C2"
        }
          
        icon_map = {
          # 2XX - Thunderstorm
          "200" : (icons["bolt"], icons["bolt"]),
          "201" : (icons["bolt"], icons["bolt"]),
          "202" : (icons["bolt"], icons["bolt"]),
          "210" : (icons["bolt"], icons["bolt"]),
          "211" : (icons["bolt"], icons["bolt"]),
          "212" : (icons["bolt"], icons["bolt"]),
          "221" : (icons["bolt"], icons["bolt"]),
          "230" : (icons["bolt"], icons["bolt"]),
          "231" : (icons["bolt"], icons["bolt"]),
          "232" : (icons["bolt"], icons["bolt"]),
          # 3XX - Drizzle
          "300" : (icons["showers"], icons["showers"]),
          "301" : (icons["showers"], icons["showers"]),
          "302" : (icons["showers"], icons["showers"]),
          "310" : (icons["showers"], icons["showers"]),
          "311" : (icons["showers"], icons["showers"]),
          "312" : (icons["showers"], icons["showers"]),
          "313" : (icons["showers_day"], icons["showers_night"]),
          "314" : (icons["showers_day"], icons["showers_night"]),
          "321" : (icons["showers_day"], icons["showers_night"]),
          # 5XX - Rain
          "500" : (icons["rain"], icons["rain"]),
          "501" : (icons["rain"], icons["rain"]),
          "502" : (icons["rain"], icons["rain"]),
          "503" : (icons["rain"], icons["rain"]),
          "504" : (icons["rain"], icons["rain"]),
          "511" : (icons["snow"], icons["snow"]),
          "520" : (icons["showers_day"], icons["showers_night"]),
          "521" : (icons["rain"], icons["rain"]),
          "522" : (icons["rain"], icons["rain"]),
          "531" : (icons["rain"], icons["rain"]),
          # 6XX - Snow
          "600" : (icons["snow"], icons["snow"]),
          "601" : (icons["snow"], icons["snow"]),
          "602" : (icons["snow"], icons["snow"]),
          "611" : (icons["snow"], icons["snow"]),
          "612" : (icons["snow"], icons["snow"]),
          "613" : (icons["snow"], icons["snow"]),
          "615" : (icons["snow"], icons["snow"]),
          "616" : (icons["snow"], icons["snow"]),
          "620" : (icons["snow"], icons["snow"]),
          "621" : (icons["snow"], icons["snow"]),
          "622" : (icons["snow"], icons["snow"]),
          # 7XX - Atmosphere
          "701" : (icons["fog"], icons["fog"]),
          "711" : (icons["fog"], icons["fog"]),
          "721" : (icons["fog"], icons["fog"]),
          "731" : (icons["fog"], icons["fog"]),
          "741" : (icons["fog"], icons["fog"]),
          "751" : (icons["fog"], icons["fog"]),
          "761" : (icons["fog"], icons["fog"]),
          "762" : (icons["fog"], icons["fog"]),
          "771" : (icons["wind"], icons["wind"]),
          "781" : (icons["wind"], icons["wind"]),
          # 800 - Clear
          "800" : (icons["clear_day"], icons["clear_night"]),
          # 8XX - Clouds
          "801" : (icons["cloudy_day"], icons["cloudy_night"]),
          "802" : (icons["cloudy_day"], icons["cloudy_night"]),
          "803" : (icons["cloudy_day"], icons["cloudy_night"]),
          "804" : (icons["cloudy"], icons["cloudy"])
        }
        
        weather_id = str(weather_data['weather'][0]['id'])
        
        now_dt = datetime.datetime.now()
        now_ux = int(datetime.datetime.timestamp(now_dt))
        sunrise_ux = int(weather_data["sys"]["sunrise"])
        sunset_ux = int(weather_data["sys"]["sunset"])
        
        if now_ux >= sunrise_ux and now_ux < sunset_ux:
            tod = 0
        else:
            tod = 1
        
        icon = icon_map[weather_id][tod]
        return(icon)
    
    def call_api(self, url, api_key, city_id):
        url_full = url + "?" + "id=" + city_id + "&" + "APPID=" + api_key
        api_reply = urlopen(url_full)
        weather_data = json.loads(api_reply.read())
        return(weather_data)


class Wgt_Load(object):
    def __init__(self, params):
        self.text_format = params["text_format"]
    
    def create_result(self):
        loadstring = self.get_loadstring(self.text_format)
        result = Result(loadstring, loadstring, "\uF3FD", False)
        return(result)
    
    def get_loadstring(self, fmt):
        load_filepath = "/proc/loadavg"
        load_file = open(load_filepath, 'r')
        load_string_full = load_file.read()
        load_file.close()
        cpu_filepath = "/proc/cpuinfo"
        cpu_file = open(cpu_filepath, 'r')
        cpu_string_full = cpu_file.read()
        cpu_count_txt = str(cpu_string_full.count("processor"))
        cpu_file.close()
        load_string_splitted = load_string_full.split(' ')
        load_1min_txt = load_string_splitted[0]
        load_5min_txt = load_string_splitted[1]
        load_15min_txt = load_string_splitted[2]
        out_txt = fmt
        out_txt = out_txt.replace("%{1}", load_1min_txt)
        out_txt = out_txt.replace("%{5}", load_5min_txt)
        out_txt = out_txt.replace("%{15}", load_15min_txt)
        out_txt = out_txt.replace("%{CNT}", cpu_count_txt)
        return(out_txt)
    
    @property
    def load_period(self):
        return self._load_period
    
    @load_period.setter
    def load_period(self, load_period):
        if load_period not in [ "1", "5", "15" ]:
            self._load_period = "0"
        else:
            self._load_period = load_period

class Wgt_CoreTemp(object):
    def __init__(self, params):
        self.core_ids = params["cores"].split(",")
    
    def create_result(self):
        coretempstring = self.get_coretempstring()
        result = Result(coretempstring, coretempstring, "\uF2C8", False)
        return(result)
    
    def get_coretempstring(self):
        core_temps = list()
        for core_id in self.core_ids:
            core_temp_file = open("/sys/class/thermal/thermal_zone" + core_id + "/temp", 'r')
            core_temp_str = core_temp_file.read().rstrip("\n")
            core_temp = int(core_temp_str) / 1000 # Change millidegree to degree
            core_temps.append(str(core_temp))
            core_temp_file.close()
        out_txt = str()
        for core_temp in core_temps:
            out_txt = out_txt + core_temp + "°C / "
        out_txt = out_txt[:-3] # Strip trailing separator.
        return(out_txt)

class Wgt_ShellCmd(object):
    def __init__(self, params):
        self.cmd = params["cmd"]
        self.icon = params["icon"] # Shit's not working, FIXME
    
    def create_result(self):
        cmdoutputstring = self.get_cmdoutputstring()
        result = Result(cmdoutputstring, cmdoutputstring, "\uF49E", False)
        return(result)
    
    def get_cmdoutputstring(self):
        cmdoutputstring = os.popen(self.cmd).read().rstrip("\n")
        return(cmdoutputstring)


def get_sysctlvalue(mib):
	cmd = '/sbin/sysctl ' + mib
	cmdoutput_raw = os.popen(cmd).read().rstrip("\n")
	cmdoutput_cut = cmdoutput_raw.split('=')[1]
	return(cmdoutput_cut)
