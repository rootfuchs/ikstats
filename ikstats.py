#!/usr/bin/env python3

import os
import sys
import time
import json
import configparser
import threading

class Widget(object):
    def __init__(self, cfg, wgt_id):
        # TODO: Get Config values by ConfigParser (bool, int, etc.)
        # Setting as read from the config
        self.id = wgt_id
        self.cfg = cfg
        self.widget = self.create_widget()
    
    def create_widget(self):
        wgt_module = __import__("widgets")
        wgt_class = getattr(wgt_module, self.cfg.cname)
        wgt_inst = wgt_class(self.cfg.params)
        return(wgt_inst)
    
    def get_final_data(self):
        self.result = self.widget.create_result()
        res_dict = self.result.get_dict()
        cfg_dict = {
          'name'                  : self.cfg.name,
          'color'                 : self.cfg.color,
          'border'                : self.cfg.border,
          'separator'             : self.cfg.separator,
          'separator_block_width' : self.cfg.separator_block_width,
          'background'            : self.cfg.background,
          'min_width'             : self.cfg.min_width,
          'align'                 : self.cfg.align,
          'markup'                : self.cfg.markup
        }
        full_dict = { **res_dict, **cfg_dict }
        return(full_dict)


class WidgetConfig(object):
    def __init__(self, config, section):
        self.name = config.get(section, 'name')
        self.cname = config.get(section, 'cname')
        self.color = config.get(section, 'color')
        self.border = config.get(section, 'border')
        self.separator = config.getboolean(section, 'separator')
        self.separator_block_width = config.get(section, 'separator_block_width')
        self.background = config.get(section, 'background')
        self.min_width = config.get(section, 'min_width')
        self.align = config.get(section, 'align')
        self.markup = config.get(section, 'markup')
        self.interval = config.get(section, 'interval')
        self.enable = config.getboolean(section, 'enable')
        self.params = dict()
        for setting in config.items(section, raw=True):
            if setting[0].startswith("param.") == True:
                param_val = config.get(section, setting[0], raw=True)
                param_key = setting[0][6:] # FIXME - Look for a smarter way (something like strip() or so)
                self.params[param_key] = param_val
        
    @property
    def color(self):
        return(self._color)
    
    @color.setter
    def color(self, color):
        if len(color) == 7 and color[0] == '#':
            try:
                int(color[1:6], 16)
            except:
                raise ValueError("Error in config file: \'color\' must be a hex value formated \'#RRGGBB\'")
            self._color = color
        else:
            raise ValueError("Error in config file: \'color\' must be a hex value formated \'#RRGGBB\'")
    
    @property
    def background(self):
        return(self._background)
    
    @background.setter
    def background(self, color):
        if len(color) == 7 and color[0] == '#':
            try:
                int(color[1:6], 16)
            except:
                raise ValueError("Error in config file: \'background\' must be a hex value formated \'#RRGGBB\'")
            self._background = color
        else:
            raise ValueError("Error in config file: \'background\' must be a hex value formated \'#RRGGBB\'")
    
    @property
    def border(self):
        return(self._border)
    
    @border.setter
    def border(self, color):
        if len(color) == 7 and color[0] == '#':
            try:
                int(color[1:6], 16)
            except:
                raise ValueError("Error in config file: \'border\' must be a hex value formated \'#RRGGBB\'")
            self._border = color
        else:
            raise ValueError("Error in config file: \'border\' must be a hex value formated \'#RRGGBB\'")
    
    @property
    def min_width(self):
        return(self._min_width)
    
    @min_width.setter
    def min_width(self, min_width):
        try:
            width = int(min_width)
        except:
            raise ValueError("Error in config file: \'min_width\' must be an integer")
        if width >= 0:
            self._min_width = width
        else:
            raise ValueError("Error in config file: \'min_width\' must be greater or equal zero")
    
    @property
    def separator_block_width(self):
        return(self._separator_block_width)
    
    @separator_block_width.setter
    def separator_block_width(self, separator_block_width):
        try:
            width = int(separator_block_width)
        except:
            raise ValueError("Error in config file: \'separator_block_width\' must be an integer")
        if width >= 0:
            self._separator_block_width = width
        else:
            raise ValueError("Error in config file: \'separator_block_width\' must be greater or equal zero")
    
    @property
    def align(self):
        return(self._align)
    
    @align.setter
    def align(self, align):
        if align in [ 'right', 'center', 'left' ]:
            self._align = align
        else:
            raise ValueError("Error in config file: \'align\' must be \'right\', \'center\' or \'left\'")
    
    @property
    def markup(self):
        return(self._markup)
    
    @markup.setter
    def markup(self, markup):
        if markup in [ 'pango', 'none' ]:
            self._markup = markup
        else:
            raise ValueError("Error in config file: \'markup\' must be \'pango\' or \'none\'")

    @property
    def interval(self):
        return(self._interval)
    
    @interval.setter
    def interval(self, interval):
        try:
            intv = int(interval)
        except:
            raise ValueError("Error in config file: \'interval\' must be an integer")
        if intv > 0:
            self._interval = intv
        else:
            raise ValueError("Error in config file: \'interval\' must be greater than zero")

class OutputStream(object):
    def __init__(self, proto_version, stop_sig, cont_sig, click_events):
        self.proto_version = proto_version
        self.stop_sig = stop_sig
        self.cont_sig = cont_sig
        self.click_events = click_events
        self.init_data = {
          'version'       : self.proto_version,
          'stop_sig'      : self.stop_sig,
          'cont_sig'      : self.cont_sig,
          'click_events'  : self.click_events
        }
        self.init_stream(self.init_data)
        self.widgets_data = dict()
    
    @property
    def stop_sig(self):
        return(self._stop_sig)
    
    @stop_sig.setter
    def stop_sig(self, stop_sig):
        try:
            sig = int(stop_sig)
        except ValueError:
            raise ValueError("Error in config file: \'stop_sig\' must be an integer value")
        self._stop_sig = sig

    @property
    def cont_sig(self):
        return(self._cont_sig)
    
    @cont_sig.setter
    def cont_sig(self, cont_sig):
        try:
            sig = int(cont_sig)
        except ValueError:
            raise ValueError("Error in config file: \'cont_sig\' must be an integer value")
        self._cont_sig = sig
    
    def init_stream(self, init_data):
        txt_output = json.dumps(init_data, ensure_ascii=False)
        print(txt_output, flush=True)
        print("[", flush=True)
    
    def widgets_stream(self, widgets_data):
        widget_output = list()
        for key in sorted(widgets_data):
            widget_output.append(widgets_data[key])
        txt_output = json.dumps(widget_output, ensure_ascii=False)
        print(txt_output + ",", flush=True)
    
    def update_widget_data(self, widget_id, widget_data):
        self.widgets_data[widget_id] = widget_data
        self.widgets_stream(self.widgets_data)
    

class WidgetHandler(object):
    def __init__(self, wgts_cfg):
        self.streamer = OutputStream(1, 10, 12, False)
        self.widgets = list()
        self.threads = list()
        id_count = 0
        for wgt_cfg in wgts_cfg:
            if wgt_cfg.enable == True:
                widget = Widget(wgt_cfg, id_count)
                self.widgets.append(widget)
                self.threads.append(self.create_wgt_thread(widget, wgt_cfg.interval))
                id_count = id_count + 1
    
    def create_wgt_thread(self, widget, interval):
        thread = threading.Thread(target=self.wgt_loop, args=(widget, interval))
        thread.start()
        return(thread)
    
    def wgt_loop(self, widget, interval): 
        while True:
            result = widget.get_final_data()
            self.streamer.update_widget_data(widget.id, result)
            time.sleep(interval)

def main():
    # Placeholder vars
    widgets = list()
    
    cfg_full = configparser.ConfigParser()
    cfg_full.read(os.path.dirname(sys.argv[0]) + '/ikstats.ini')
    
    # Copy the config structure so it can be handled more easily
    cfg_sections = cfg_full.sections()
    
    # Get global settings located in section 'main'...
    settings_version = cfg_full.get('main', 'version')
    settings_refresh = cfg_full.getint('main', 'refresh')
    settings_click = cfg_full.getboolean('main', 'click_events')
    
    # ...and remove the 'main' section from the list, so there are only
    # configs for widgets left.
    cfg_sections.remove('main')
    
    widget_cfgs = list()
    for widget_section in cfg_sections:
        widget_cfgs.append(WidgetConfig(cfg_full, widget_section))
    
    wgt_handler = WidgetHandler(widget_cfgs)
    

if __name__ == '__main__':
    main()
